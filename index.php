<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="font/css/font-awesome.min.css">
</head>
<body>
  <?php
  include('nav.php');
  ?>
    <div class="bg-atas">
        <div class="title">
            <p class="h1 text-white m-lg-auto"><b>SELAMAT DATANG DI WEB WISATA KOTA BENGKULU</b></p>
        </div>
    </div>
    <div class="bawah">
        <p class="h5 text-white" style="text-align: center;">Scrol <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-arrow-down-squarek"  viewBox="0 0 16 16">
          <path fill-rule="evenodd" d="M15 2a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2zM0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm8.5 2.5a.5.5 0 0 0-1 0v5.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V4.5z"/>
        </svg></p>
    </div>
 
	<!-- Api Corona -->
	<div class="container space">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title text-center">
					<h1>Destinasi Populer</h1>
					<div class="line-title"></div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="card">
							<a href="#">
						  		<img src="img/danau.jpg" class="card-img-top" alt="">
					  		</a>
						  	<div class="card-body">
						  		<a href="">
						    		<h5 class="card-title">Danau Dendam Tak Sudah</h5>
						    	</a>
						    		<p class="card-text">Danau Dendam Tak Sudah adalah sebuah danau yang terletak di Provinsi Bengkulu. Danau ini berlokasi di Kelurahan Dusun Besar, Kecamatan Singaran Pati, Kota Bengkulu, Provinsi Bengkulu. Danau Dendam Tak Sudah memiliki luas keseluruhan 559 dan luas permukaan 68hektare...</p>
						    	<a href="#" class="btn btn-primary">Baca lebih lanjut...</a>
						  	</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="card">
							<a href="#">
						  		<img src="img/Benteng-Marlborough.jpg" alt="" class="card-img-top">
					  		</a>
						  	<div class="card-body">
						  		<a href="">
						    		<h5 class="card-title">Benteng Marlborough</h5>
						    	</a>
						    		<p class="card-text">Benteng Marlborough adalah benteng peninggalan Inggris di Kota Bengkulu. Benteng ini didirikan oleh East India Company tahun 1714-1719 di bawah pimpinan gubernur Joseph Callet sebagai benteng pertahanan Inggris. Benteng ini didirikan di atas bukit buatan, menghadap ke arah Kota Bengkulu dan memunggungi Samudra Hindia...</p>
						    	<a href="#" class="btn btn-primary">Baca lebih lanjut...</a>
						  	</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="card">
							<a href="#">
						  		<img src="img/pantai panjang 4.jpg" class="card-img-top" alt="..." >
					  		</a>
						  	<div class="card-body">
						  		<a href="">
						    		<h5 class="card-title">Pantai Panjang</h5>
						    	</a>
						    		<p class="card-text">Pantai Panjang merupakan pantai yang berada di Provinsi Bengkulu. Pantai ini memiliki garis pantai yang mencapai 7 km dan lebar pantai sekitar 500 meter...</p>
						    	<a href="#" class="btn btn-primary">Baca lebih lanjut...</a>
						  	</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-12">
						<div class="card bg-dark text-white">
							<div class="text-center space-instagram">
								<h3>Instagram @liburinajaa</h3>
								<div class="row" id="media-ig" style="padding: 20px;"></div>
								<a href="https://www.instagram.com/liburinajaa/" role="button" class="btn btn-outline-light" target="_blank">Follow</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Section Footer -->
	<div class="footer text-white">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-lg-4 col-md-6 col-sm-12">
					<h2>Liburin Aja</h2>
					<p class="p-footer">Platform wisata dan komunitas dengan beragam tips liburan, dan kuliner sepulau jawa. Jangan lupa Subscribe & Like agar kamu terupdate. Jangan Lupa Liburan!</p>
					<div class="container">
						<div class="row pt-4">
							<div class="col-12">
								<p class="content display-5">Share :</p>
							</div>
							<div class="col-12 social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-whatsapp"></i></a>
								<a href="#"><i class="fa fa-instagram"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12" style="margin-bottom: 15px;">
							<h2>Menu</h2>
							<ul>
								<li class="footer-item">
					                <a class="footer-link text-white" href="#">Tips</a>
					            </li>
					            <li class="footer-item">
					                <a class="footer-link text-white" href="#">Kuliner</a>
					            </li> 
					            <li class="footer-item">
					                <a class="footer-link text-white" href="#">Contact Us</a>
					            </li>
							</ul>
						</div>
						<div class="col-lg6 col-md-6 col-sm-12">
							<h2>Let's Connect</h2>
							<ul>
								<li class="footer-socmed">
					                <a class="footer-link text-white" href="#"><ion-icon name="logo-facebook"></ion-icon> Instagram</a>
					            </li>
							</ul>
						</div>
					</div>
				</div>
       <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

</body>
<?php
include('footer.html');
?>
</html>